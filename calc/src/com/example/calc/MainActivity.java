package com.example.calc;





import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity    {
private Button a1; 
private Button a2;
private Button a3;
private Button a4;
private Button a5;
private Button a6;
private Button a0;
private Button ap;
private Button ar;
private EditText et;
private StringBuffer s= new StringBuffer();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		a1=(Button)this.findViewById(R.id.but1);
		a2=(Button)this.findViewById(R.id.but2);
		a3=(Button)this.findViewById(R.id.but3);
		a4=(Button)this.findViewById(R.id.but4);
		a5=(Button)this.findViewById(R.id.but5);
		a6=(Button)this.findViewById(R.id.but6);
		ap=(Button)this.findViewById(R.id.butp);
		a0=(Button)this.findViewById(R.id.but0);
		ar=(Button)this.findViewById(R.id.butr);
		et=(EditText)this.findViewById(R.id.editText1);
		
		a1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				s.append("1");}
		});
		
		a2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				s.append("2");}
		});
		
		a3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				s.append("3");}
		});
		a4.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				s.append("4");}
		});
		a5.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				s.append("5");}
		});
		a6.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				s.append("6");}
		});
		a0.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				s.append("0");}
		});
		ap.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(s.length()!=0){
			Integer r=Integer.parseInt(et.getText().toString())+Integer.parseInt(s.toString());
				et.setText(r.toString());
				s.setLength(0);}
				}
		});
		ar.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(s.length()!=0){
			Integer r=Integer.parseInt(et.getText().toString())+Integer.parseInt(s.toString());
				et.setText(r.toString());
				s.setLength(0);}
				}
		});
		
	}
	//Toast.makeText(MainActivity.this, et.getText().toString(), Toast.LENGTH_LONG).show();
		
		
		
		
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;	
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	
}
